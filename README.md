## » Leírás

A Szoftvertervezés– és Fejlesztés II. tárgyhoz tartozó laboranyagok kódjai és példái.

Az anyagok megtalálhatók a http://users.nik.uni-obuda.hu/siposm/ elérhetőségemen is, célszerű azokat is megnézni.

---

## » Letöltés

1.) .zip letöltés: Downloads nézet » Download repository

2.) cli » cél mappa » git clone https://bitbucket.org/siposm/oktatas-sztf2-18192

---

Meglátás / észrevétel / probléma esetén megtalálható vagyok az alábbi elérhetőségen.

---

	Sipos Miklós
	Tanszéki Mérnök
	sipos.miklos@nik.uni-obuda.hu
	Óbudai Egyetem Neumann János Informatikai Kar
	Alkalmazott Informaitkai Intézet
	2018 - 2019 - 2 félév
	(C)