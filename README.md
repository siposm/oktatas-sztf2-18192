## SzTF II.

A Szoftvertervezés– és Fejlesztés II. tárgyhoz tartozó laboranyagok kódjai és példái. Hasonló és egyéb anyagok megtalálhatók a http://users.nik.uni-obuda.hu/siposm/ elérhetőségemen is, célszerű azokat is megnézni.

## Használat
### Letöltés
Clone gomb alatt a felhőre katt, mentés a kiválaszott állományban.

### Git
Töltsd le, telepítsd, indítsd el: https://git-scm.com/downloads
    
`cd ~/Desktop/`\
`mkdir sztf2-laboranyag`\
`cd sztf2-laboranyag`\
`git clone https://gitlab.com/siposm/oktatas-sztf2-18192.git`

---

Meglátás / észrevétel / probléma esetén megtalálható vagyok az alábbi elérhetőségen.


**Sipos Miklós**\
Tanszéki Mérnök\
sipos.miklos@nik.uni-obuda.hu\
Óbudai Egyetem Neumann János Informatikai Kar\
Szoftvertervezés és -Fejlesztés Intézet\
2018 - 2019 - 2 félév\
(C)
